package com.projet.calculateur.calcservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class ApplicationCalculateur {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationCalculateur.class, args);
	}
}
