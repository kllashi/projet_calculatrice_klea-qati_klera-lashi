package com.projet.calculateur.calcservice.handler;

public interface MessageHandler {

	String process(String message);
	
}
