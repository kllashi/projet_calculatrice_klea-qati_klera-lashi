package com.projet.calculateur.calcservice.serveur;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CalculateurServeur implements CommandLineRunner {
	
	@Autowired
	private ServeurRabbit serveurRabbit;

	@Override
	public void run(String... args) throws Exception {
		logger.info("Démarrant ServeurRabbit...");
		serveurRabbit.start();
	}

	private static final Logger logger = LoggerFactory.getLogger(CalculateurServeur.class);
}
