package com.projet.calculateur.calcservice.operateurs;

import java.math.BigDecimal;

public class Soustraction implements Operation {

	public BigDecimal calculer(BigDecimal a, BigDecimal b) {
		return a.subtract(b);
	}
}
