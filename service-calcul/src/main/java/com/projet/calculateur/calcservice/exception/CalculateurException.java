package com.projet.calculateur.calcservice.exception;

public class CalculateurException extends RuntimeException {

	private static final long serialVersionUID = 1L;	

	public CalculateurException() {
		super();
	}

	public CalculateurException(String s) {
		super(s);
	}

	public CalculateurException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public CalculateurException(Throwable throwable) {
		super(throwable);
	}	
		
}
