package com.projet.calculateur.calcservice.serveur;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.projet.calculateur.calcservice.handler.MessageHandler;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

@Component
public class ServeurRabbit {

	private ConnectionFactory factory = new ConnectionFactory();
	private boolean stop = false;
	@Autowired
	private MessageHandler messageHandler;
	private Connection connexion = null;

	public void start() {
		connexion = null;
		Channel channel = null;
		try {
			factory.setHost("localhost");
			connexion = factory.newConnection();
			channel = connexion.createChannel();
			channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
			channel.basicQos(1);

			QueueingConsumer consommateur = new QueueingConsumer(channel);
			channel.basicConsume(RPC_QUEUE_NAME, false, consommateur);

			logger.info(">>>>> En attendant les requêtes <<<<<");

			while (!stop) {
				String response = null;
				QueueingConsumer.Delivery delivery = consommateur.nextDelivery();
				BasicProperties proprietes = delivery.getProperties();
				String corrId = proprietes.getCorrelationId();
				BasicProperties proprietesReponse = new BasicProperties.Builder().correlationId(corrId).build();
				
				try {
					String message = new String(delivery.getBody(),"UTF-8");
					logger.info(String.format("<-- Réponse par %s : %s", corrId, message));
					
					response = messageHandler.process(message);
					
					logger.info(String.format("--> Envoyer à %s : %s", corrId, response));
				}
				catch (Exception e){
					response = String.format("ERROR: %s", e.getMessage());
					logger.info(String.format("--> Envoyer à %s : %s", corrId, response));
				}
				finally {  
					channel.basicPublish( "", proprietes.getReplyTo(), proprietesReponse, response.getBytes("UTF-8"));
					channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
				}
			}
		}
		catch  (Exception e) {
			logger.error("Une erreur est survenue !", e);
		}
		finally {
			stop();
		}
    }
	
	public void stop() {
		stop = true;
		
		if (connexion != null) {
			try {
				connexion.close();
			}
			catch (Exception ignore) {}
		}
	}
	
	private static final String RPC_QUEUE_NAME = "rpc_queue";
	
	private static final Logger logger = LoggerFactory.getLogger(ServeurRabbit.class);
}
