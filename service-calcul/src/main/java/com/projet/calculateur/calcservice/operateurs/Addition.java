package com.projet.calculateur.calcservice.operateurs;

import java.math.BigDecimal;

public class Addition implements Operation {

	public BigDecimal calculer(BigDecimal a, BigDecimal b) {
		return a.add(b);
	}
}
