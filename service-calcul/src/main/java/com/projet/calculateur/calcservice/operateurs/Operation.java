package com.projet.calculateur.calcservice.operateurs;

import java.math.BigDecimal;

public interface Operation {

	BigDecimal calculer(BigDecimal a, BigDecimal b);
	
}
