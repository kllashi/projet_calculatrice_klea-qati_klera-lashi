package com.projet.calculateur.calcservice.operateurs;

import java.math.BigDecimal;

public class Multiplication implements Operation {

	public BigDecimal calculer(BigDecimal a, BigDecimal b) {
		return a.multiply(b);
	}

}
