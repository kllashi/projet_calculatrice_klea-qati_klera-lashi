package com.projet.calculateur.calcservice.operateurs;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Division implements Operation {

	public BigDecimal calculer(BigDecimal a, BigDecimal b) {
		if (b.equals(BigDecimal.ZERO))
			throw new IllegalArgumentException("Division par 0");
		
		return a.divide(b, 6, RoundingMode.CEILING);
	}
}
