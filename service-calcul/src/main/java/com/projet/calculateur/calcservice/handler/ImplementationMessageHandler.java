package com.projet.calculateur.calcservice.handler;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.projet.calculateur.calcservice.exception.CalculateurException;
import com.projet.calculateur.calcservice.operateurs.Operation;

@Component
public class ImplementationMessageHandler implements MessageHandler {
	
	public ImplementationMessageHandler() {}	

	@Override
	public String process(String message) {
		String resultat = null;
		
		try { 
			String[] arrMessage = message.split(",");		
			Operation oper = creerObjet(arrMessage[0]);
			BigDecimal a = new BigDecimal(arrMessage[1]);
			BigDecimal b = new BigDecimal(arrMessage[2]);
			resultat = oper.calculer(a, b).toString();
		}
		catch (ArrayIndexOutOfBoundsException e) {
			String exceptionMessage = String.format("Le format de message n'est pas correct ! '%s'", message);
			logger.error(exceptionMessage);
			throw new CalculateurException(exceptionMessage + " Usage: 'OPER,A,B'", e);
		}
		catch (ClassNotFoundException e) {
			String exceptionMessage = String.format("L'opération est inconnue ! '%s'", message);
			logger.error(exceptionMessage);
			throw new CalculateurException(exceptionMessage, e);
		}
		catch (InstantiationException e) {
			String exceptionMessage = String.format("Impossible d'instancier l'opération !", e.getMessage());
			logger.error(exceptionMessage);
			throw new CalculateurException(exceptionMessage, e);
		}
		catch (IllegalAccessException e) {
			logger.error(e.getMessage());
			throw new CalculateurException(e);
		}
		
		return resultat;
	}	

	private static Operation creerObjet(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> cls = Class.forName(operPackage + "." + className);
		return (Operation) cls.newInstance();
	}
	
	private static String operPackage = "com.projet.calculateur.calcservice.operateurs";
	
	private static final Logger logger = LoggerFactory.getLogger(ImplementationMessageHandler.class);

}
