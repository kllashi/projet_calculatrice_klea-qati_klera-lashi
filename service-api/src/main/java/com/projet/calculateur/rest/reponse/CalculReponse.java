package com.projet.calculateur.rest.reponse;

import java.math.BigDecimal;

public class CalculReponse {

	private final BigDecimal resultat;
	
	public CalculReponse(BigDecimal resultat) {
		this.resultat = resultat;
	}
	
	public CalculReponse(String resultat) {
		this.resultat = new BigDecimal(resultat);
	}

	public BigDecimal getResult() {
		return resultat;
	}
	
}
