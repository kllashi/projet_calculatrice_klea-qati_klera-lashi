package com.projet.calculateur.rest.service;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.projet.calculateur.rest.client.ClientRabbit;
import com.projet.calculateur.rest.exception.RestException;
import com.projet.calculateur.rest.reponse.CalculReponse;

@ComponentScan
@Service
public class RabbitServiceImplementation implements RabbitService {
	@Autowired
	private ClientRabbit clientRabbit;

	@Override
	public CalculReponse send(String operation, BigDecimal a, BigDecimal b) {
		String message = String.format("%s,%s,%s", operation, a, b);
		
		String reponse = null;
		try {
			reponse = clientRabbit.call(message);
		}
		catch (Exception e) {
			logger.error("Une erreur est survenue !", e);
			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, "Incapable de compléter la requête.");
		}
		
		if ( reponse.startsWith("ERROR") )
			throw new RestException(HttpStatus.BAD_REQUEST, reponse);		
		
		return new CalculReponse(reponse);
	}

	private static final Logger logger = LoggerFactory.getLogger(RabbitServiceImplementation.class);

}
