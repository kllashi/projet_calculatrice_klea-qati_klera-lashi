package com.projet.calculateur.rest.controleur;

import com.projet.calculateur.rest.entities.Calcul;
import com.projet.calculateur.rest.reponse.CalculReponse;
import com.projet.calculateur.rest.service.RabbitService;
import com.projet.calculateur.rest.service.ServiceCalcul;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/computations"})
public class ControleurCalculatrice {
 
	@Autowired
	private RabbitService rabbitService;

	@Autowired
	private ServiceCalcul serviceCalcul;

	@GetMapping({"/add"})
	public ResponseEntity<String> add(@RequestParam("a") BigDecimal a, @RequestParam("b") BigDecimal b) {
		CalculReponse calcResponse = this.rabbitService.send("Addition", a, b);
		BigDecimal resultat = calcResponse.getResult();
		Calcul calculAddition = new Calcul(a, b, "+", resultat);
		calculAddition.setId(UUID.randomUUID().toString());
		this.serviceCalcul.save(calculAddition);

		return new ResponseEntity("ID: " + calculAddition.getId(), HttpStatus.ACCEPTED);
	}

	@GetMapping({"/sous"})
	public ResponseEntity<String> sous(@RequestParam("a") BigDecimal a, @RequestParam("b") BigDecimal b) {
		CalculReponse calcResponse = this.rabbitService.send("Soustraction", a, b);
		BigDecimal resultat = calcResponse.getResult();
		Calcul calculSoustraction = new Calcul(a, b, "-", resultat);
		calculSoustraction.setId(UUID.randomUUID().toString());
		this.serviceCalcul.save(calculSoustraction);
		
		return new ResponseEntity("ID: " + calculSoustraction.getId(), HttpStatus.ACCEPTED);
	}

	@GetMapping({"/mult"})
	public ResponseEntity<String> mult(@RequestParam("a") BigDecimal a, @RequestParam("b") BigDecimal b) {
		CalculReponse calcResponse = this.rabbitService.send("Multiplication", a, b);
		BigDecimal resultat = calcResponse.getResult();
		Calcul calculMultiplication = new Calcul(a, b, "*", resultat);
		calculMultiplication.setId(UUID.randomUUID().toString());
		this.serviceCalcul.save(calculMultiplication);
		
		return new ResponseEntity("ID :" + calculMultiplication.getId(), HttpStatus.ACCEPTED);
	}

	@GetMapping({"/div"})
	public ResponseEntity<String> div(@RequestParam("a") BigDecimal a, @RequestParam("b") BigDecimal b) {
		CalculReponse calcResponse = this.rabbitService.send("Division", a, b);
		BigDecimal resultat = calcResponse.getResult();
		Calcul calculDivision = new Calcul(a, b, "/", resultat);
		calculDivision.setId(UUID.randomUUID().toString());
		this.serviceCalcul.save(calculDivision);
		
		return new ResponseEntity("ID :" + calculDivision.getId(), HttpStatus.ACCEPTED);
	}

	@GetMapping(value = {"/{id}"}, produces = {"application/json"})
	public ResponseEntity<Calcul> getCalcul(@PathVariable("id") String calculId) {
		Calcul c = this.serviceCalcul.findById(calculId);
		
		return new ResponseEntity(c, HttpStatus.OK);
	}

	@ExceptionHandler({IllegalArgumentException.class, NullPointerException.class})
	void handleBadRequests(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value(), "Paramètre invalide !");
	}
}
