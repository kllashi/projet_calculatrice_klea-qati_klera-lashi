package com.projet.calculateur.rest.entities;

import java.io.Serializable;
import java.math.BigDecimal;

public class Calcul implements Serializable {

	private String id;

	private BigDecimal nombre1;

	private BigDecimal nombre2;

	private String operation;

	private BigDecimal resultat;

	public Calcul(BigDecimal nombre1, BigDecimal nombre2, String operation, BigDecimal resultat) {
		this.nombre1 = nombre1;
		this.nombre2 = nombre2;
		this.operation = operation;
		this.resultat = resultat;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getNombre1() {
		return this.nombre1;
	}

	public void setNombre1(BigDecimal nombre1) {
		this.nombre1 = nombre1;
	}

	public BigDecimal getNombre2() {
		return this.nombre2;
	}

	public void setNombre2(BigDecimal nombre2) {
		this.nombre2 = nombre2;
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public BigDecimal getResultat() {
		return this.resultat;
	}

	public void setResultat(BigDecimal resultat) {
		this.resultat = resultat;
	}
}
