package com.projet.calculateur.rest.service;

import com.projet.calculateur.rest.entities.Calcul;
import java.util.UUID;
import javax.ws.rs.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class ServiceCalcul {
	@Autowired
	private RedisTemplate<String, Calcul> calculTemplate;

	public Calcul findById(String calculId) {
		Calcul calcul = (Calcul)getValueOperations().get(getRedisKey(UUID.fromString(calculId).toString()));
		if (calcul == null)
		  throw new NotFoundException("Calcul n'existe pas dans la BDD !"); 
		
		return calcul;
	}

	public void save(final Calcul calcul) {
		getValueOperations().set(getRedisKey(calcul.getId()), calcul);
	}

	private String getRedisKey(final String calculId) {
		return calculId;
	}

	private ValueOperations<String, Calcul> getValueOperations() {
		return calculTemplate.opsForValue();
	}
}
