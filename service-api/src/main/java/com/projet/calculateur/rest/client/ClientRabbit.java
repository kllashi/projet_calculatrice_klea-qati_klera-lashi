package com.projet.calculateur.rest.client;

import java.util.UUID;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

@Component
public class ClientRabbit {

	private static ConnectionFactory connexionFactory = new ConnectionFactory();	
	
	private Connection connexion;
	private Channel channel;
	private String nomRequeteQueue = "rpc_queue";
	private String nomReponseQueue;
	private QueueingConsumer consommateur;

	public ClientRabbit() throws Exception {
		connexionFactory.setHost("localhost");
		connexion = connexionFactory.newConnection();
		channel = connexion.createChannel();

		nomReponseQueue = channel.queueDeclare().getQueue();
		consommateur = new QueueingConsumer(channel);
		channel.basicConsume(nomReponseQueue, true, consommateur);
	}

	public String call(String message) throws Exception {
		String reponse = null;
		
		String corrId = (String) MDC.get("UNIQUE_ID");
		if ( corrId == null )
			corrId = UUID.randomUUID().toString();
		
		logger.info(String.format("%s --> Requesting : %s", corrId, message));
		
		BasicProperties proprietes = new BasicProperties
				.Builder().correlationId(corrId).replyTo(nomReponseQueue).build();

		channel.basicPublish("", nomRequeteQueue, proprietes, message.getBytes("UTF-8"));

		while (true) {
			QueueingConsumer.Delivery delivery = consommateur.nextDelivery();
			if (delivery.getProperties().getCorrelationId().equals(corrId)) {
				reponse = new String(delivery.getBody(),"UTF-8");
				break;
			}
		}
		
		logger.info(String.format("%s <-- Got : %s", corrId, reponse));

		return reponse;
	}

	public void close() throws Exception {
		connexion.close();
	}
	
	private static final Logger logger = LoggerFactory.getLogger(ClientRabbit.class);
}
