package com.projet.calculateur.rest.service;

import java.math.BigDecimal;

import com.projet.calculateur.rest.reponse.CalculReponse;

public interface RabbitService {

	CalculReponse send(String operation, BigDecimal a, BigDecimal b);
	
}
