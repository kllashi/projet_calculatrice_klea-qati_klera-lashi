package com.projet.calculateur.rest;

import com.projet.calculateur.rest.entities.Calcul;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@SpringBootApplication
@EnableRedisRepositories
@ComponentScan
@Configuration
public class CalculateurApp {

	@Value("localhost")
	private String redisHostname;

	@Value("6379")
	private int redisPort;

	public static void main(String[] args) {
		SpringApplication.run(com.projet.calculateur.rest.CalculateurApp.class, args);
	}

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(this.redisHostname, this.redisPort);
		
		return new JedisConnectionFactory(redisStandaloneConfiguration);
	}

	@Bean
	public RedisTemplate<String, Calcul> calculTemplate() {
		RedisTemplate<String, Calcul> template = new RedisTemplate();
		template.setConnectionFactory((RedisConnectionFactory)jedisConnectionFactory());
		template.setKeySerializer((RedisSerializer)new StringRedisSerializer());
		
		return template;
	}
}

